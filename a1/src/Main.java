import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        try
        {
            System.out.println("\nInput an integer whose factorial will be computed:");
            int num = sc.nextInt();
            int factorial = 1;

            for (int x = 1; x <= num; x++)
            {
                factorial *= x;
            }

            System.out.println("The factorial of " + num + " is " + factorial);
        }
        catch(Exception e)
        {
            System.out.println("Only numbers are accepted for this system");
        }
    }
}
