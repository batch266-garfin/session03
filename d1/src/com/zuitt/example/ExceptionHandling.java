package com.zuitt.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {

    public static void main(String[] args) {

        // Exceptions
        // A problem that arises during the execution of the program
        // It disrupts the normal flow of the program and terminates abnormally.

        // Exception Handling
        // - refers to managing and catching run-time errors in order to safely run your code.

        Scanner input = new Scanner(System.in);

        int num = 0;

        System.out.println("Enter a number: ");

        // try-catch-finally

        try
        {
            num = input.nextInt();
        }
        catch(ArithmeticException e)
        {
            System.out.println("You cannot divide a whole number by zero.");
        }
        catch(InputMismatchException e)
        {
            System.out.println("Please input numbers only.");
        }
        catch(Exception e)
        {
            System.out.println("Something went wrong. Please try again!");
            //e.printStackTrace();
        }
        finally
        {
            System.out.println("This will execute no matter what.");
        }


    }
}
