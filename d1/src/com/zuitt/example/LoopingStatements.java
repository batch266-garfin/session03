package com.zuitt.example;

public class LoopingStatements {

    public static void main(String[] args) {

        // Loops
        // - are control structures that allows codeblocks to be executed multiple times

        // While Loop
        // - allows repetitive use of code, similar to for-loops, but usually used for situation where the content to iterate is indefinite.
        // Increment - (++) add 1
        // Decrement - (--) subtract 1

        int x = 0;

        while(x < 1)
        {
            System.out.println("Loop Number: " + x);
            x++;
        }

        // Do-while Loop
        // - similar to while loop. However, do-while loop will always execute at lest once - while loops may not execute at all.

        int y = 1;

        do
        {
            System.out.println("Countdown: " + y);
            y--;
        }
        while(y > 0);

        // For Loop
        // - for(initialValue; condition; iteration)
        // {
        //  statement
        // }

        for (int i = 0; i < 1; i++)
        {
            System.out.println("Current count: " + i);
        }

        for (char f = 'a'; f < 'b'; f++)
        {
            System.out.println("Letter: " + f);
        }

        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for (int row = 0; row < 3; row++)
        {
            for (int col = 0; col < 3; col++)
            {
                System.out.println("" + classroom[row][col]);
            }
        }













    }
}
